#!/bin/php
<?php
declare(strict_types = 1);

const SUCCESS = 0;
const ERROR_PARAMS = 10;
const ERROR_INPUT = 11;
const ERROR_OUTPUT = 12;
const ERROR_INTERNAL = 99;

class TestCase {
    public $source;
    public $input;
    public $output;
    public $return_code;

    public $executed = false;
    public $status = false;

    public function generate_report() {
        printf("TestCase: generate_report()\n");
    }
}

class TestExecutor {
    public $parser = "parser.php";
    public $interpret = "interpret.py";
    public $skip_parser = false;
    public $skip_interpret = false;

    public function run_test(TestCase &$test_case) {
        printf("TestExecutor: run_test()\n");

        $test_case->executed = true;
    }
}

class TestIndexer {
    public $rootdir = "";
    public $recursive = false;

    private $test_cases = array();
    private $configured = false;

    public function gather_tests() : bool {
        if (!$this->configured) {
            if (!$this->configure()) {
                return false;
            }
        }
        printf("TestIndexer: gather_tests()\n");

        return true;
    }

    public function get_tests() {
        printf("TestIndexer: get_tests()\n");
        return $this->test_cases;
    }

    private function configure() : bool {
        printf("TestIndexer: configure()\n");

        return true;
    }
}

class TestReporter {
    private $test_cases;

    public function generate_report() {
        printf("TestReporter: generate_report()\n");
    }
}

function usage() {
    printf("test: 123\n");
}

$shortopts = "h";
$longopts  = array(
    "help",
    "directory:",
    "recursive",
    "parse-script:",
    "int-script:",
    "parse-only",
    "int-only",
    "jexamxml:",
    "jexamcfg:"
);

$options = getopt($shortopts, $longopts);

if (isset($options["h"]) || isset($options["help"])) {
    if (count($options) > 1) {
        exit(ERROR_PARAMS);
    }

    usage();

    exit(SUCCESS);
}

$test_indexer = new TestIndexer();
$test_executor = new TestExecutor();

if (isset($options["directory"])) {
    $test_indexer->rootdir = $options["directory"];
}

if (isset($options["recursive"])) {
    $test_indexer->recursive = true;
}

if (isset($options["parse-script"])) {
    $test_executor->parser = $options["parse-script"];
}

if (isset($options["int-script"])) {
    $test_executor->interpret = $options["int-script"];
}

if (isset($options["int-only"])) {
    if (isset($options["parse-only"]) || isset($options["parse-script"])) {
        error_log("Error: Parameter --int-only is mutually exclusive with --parse-only & --parse-script\n");
        exit(ERROR_PARAMS);
    }

    $test_executor->skip_parser = true;
}

if (isset($options["parse-only"])) {
    if (isset($options["int-only"]) || isset($options["int-script"])) {
        error_log("Error: Parameter --parse-only is mutually exclusive with --int-only & --int-script\n");
        exit(ERROR_PARAMS);
    }

    $test_executor->skip_interpret = true;
}

if (!$test_indexer->gather_tests()) {
    error_log("Error: Could not gather test cases\n");
    exit(ERROR_INPUT);
}

$test_cases = $test_indexer->get_tests();

foreach ($test_cases as &$test_case) {
    $test_executor->run_test($test_case);
}

$test_reporter = new TestReporter($test_cases);
$test_reporter->generate_report();

exit(SUCCESS);
?>
